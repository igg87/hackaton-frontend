import { LitElement, html, css } from 'lit-element'

class PersonaRating extends LitElement {

    static get properties () {
        return {
            rating: {type: Number}
        }
    }
    
    static get styles () {
        return css`
            .star {
                color: orange;
                font-size: 24pt;
            }
            .centered {
                text-align: center;
            }
            .d-none {
                display: none;
            }
            .fa-star {
              color: orange;
            }
            html, input, h5, p, label, a, button {
                font-family: Arial, Helvetica, sans-serif;
            }
        `
    }

    render () {
        // El render pinta lo que se visualiza por pantalla
        // La comilla ` permite presentar el html en modo multi-linea
        return html`
                <span id="star1" class="fa fa-star ">☆</span>
                <span id="star2" class="fa fa-star ">☆</span>
                <span id="star3" class="fa fa-star ">☆</span>
                <span id="star4" class="fa fa-star ">☆</span>
                <span id="star5" class="fa fa-star ">☆</span>
        `
    }

    updated (changedProperties) {
        // console.log("updated de persona-rating")
        // Buscamos cambios en una variable concreta
        if (changedProperties.has("rating")) {
            // console.log("Ha cambiado el rating en persona-rating: " + this.rating)
            this.showRating()
        }
    }

    showRating () {
        // this.shadowRoot.getElementById("star1").classList.remove("d-none")
        this.shadowRoot.getElementById("star2").classList.remove("d-none")
        this.shadowRoot.getElementById("star3").classList.remove("d-none")
        this.shadowRoot.getElementById("star4").classList.remove("d-none")
        this.shadowRoot.getElementById("star5").classList.remove("d-none")
        if (this.rating <= 1) {
            this.shadowRoot.getElementById("star2").classList.add("d-none")
            this.shadowRoot.getElementById("star3").classList.add("d-none")
            this.shadowRoot.getElementById("star4").classList.add("d-none")
            this.shadowRoot.getElementById("star5").classList.add("d-none")
        } else if (this.rating === 2) {
            this.shadowRoot.getElementById("star3").classList.add("d-none")
            this.shadowRoot.getElementById("star4").classList.add("d-none")
            this.shadowRoot.getElementById("star5").classList.add("d-none")
        } else if (this.rating === 3) {
            this.shadowRoot.getElementById("star4").classList.add("d-none")
            this.shadowRoot.getElementById("star5").classList.add("d-none")
        } else if (this.rating === 4) {
            this.shadowRoot.getElementById("star5").classList.add("d-none")
        }
    }
}

customElements.define('persona-rating', PersonaRating)