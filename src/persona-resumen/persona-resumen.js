import { LitElement, html, css } from 'lit-element'

class PersonaResumen extends LitElement {

    static get properties () {
        return {
            stats: {type: Object},
            changed: {type: Boolean}
        }
    }

    constructor () {
        super()
        this.stats = {}
        this.getResumeData()
        this.changed = false
    }

    static get styles () {
        return css`
            .bold {
                font-size: 14pt;
                font-weight: bold;
            }
            .pl-5 {
                padding-left: 10px;
            }
            .d-none {
                display: none;
            }
            .mb-10 {
                margin-bottom: 10px;
            }
            .clickable {
                cursor: pointer;
                font-weight: bold;
                text-decoration: underline;
                color: blue;
                font-size: 14pt;
            }
            .star {
                color: orange;
                font-size: 14pt;
            }
            .star-transparent {
                font-size: 14pt;
                color: transparent;
            }
            html, input, h5, p, label, a, button {
                font-family: Arial, Helvetica, sans-serif;
            }
        `
    }

    render () {
        // El render pinta lo que se visualiza por pantalla
        // La comilla ` permite presentar el html en modo multi-linea
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <button id="showRes" @click="${this.showResume}" class="btn btn-link">Mostrar resumen</button>
            <div id="hideRes" class="d-none mb-10">
                <a @click="${this.hideResume}" class="btn btn-link">Ocultar resumen</a>
                </br>
                <div class="card" style="width: 18rem;">
                    <div class="card-body">
                        <label class="bold">Total empresas: </label>
                        <span class="pl-5 clickable" @click="${this.clearFilters}">${this.stats.total}</span>
                        <br></br>
                        <span class="star">☆☆☆☆☆</span>
                        <span class="pl-5 clickable" @click="${this.filterFive}">${this.stats.fiveStarsBusiness}</span>
                        <br></br>
                        <span class="star">☆☆☆☆</span><span class="star-transparent">☆</span>
                        <span class="pl-5 clickable" @click="${this.filterFour}">${this.stats.fourStarsBusiness}</span>
                        <br></br>
                        <span class="star">☆☆☆</span><span class="star-transparent">☆☆</span>
                        <span class="pl-5 clickable" @click="${this.filterThree}">${this.stats.threeStarsBusiness}</span>
                        <br></br>
                        <span class="star">☆☆</span><span class="star-transparent">☆☆☆</span>
                        <span class="pl-5 clickable" @click="${this.filterTwo}">${this.stats.twoStarsBusiness}</span>
                        <br></br>
                        <span class="star">☆</span><span class="star-transparent">☆☆☆☆</span>
                        <span class="pl-5 clickable" @click="${this.filterOne}">${this.stats.oneStarBusiness}</span>
                    </div>
                </div>
            </div>
        `
    }

    updated (changedProperties) {
        console.log("-- changedProperties")
        if (changedProperties.has("changed")) {
            console.log("-- changedProperties")
            this.getResumeData()
            this.changed = false
        }
        
    }

    clearFilters () {
        this.emitFilter("http://localhost:8081/ecobusiness/v1/business")
    }

    filterOne () {
        this.emitFilter("http://localhost:8081/ecobusiness/v1/business?rating=1")
    }

    filterTwo () {
        this.emitFilter("http://localhost:8081/ecobusiness/v1/business?rating=2")
    }

    filterThree () {
        this.emitFilter("http://localhost:8081/ecobusiness/v1/business?rating=3")
    }

    filterFour () {
        this.emitFilter("http://localhost:8081/ecobusiness/v1/business?rating=4")
    }

    filterFive () {
        this.emitFilter("http://localhost:8081/ecobusiness/v1/business?rating=5")
    }

    emitFilter (urlService) {
        this.dispatchEvent(new CustomEvent("persona-filter", {
            "detail": {
                "url": urlService
            }
        }))
    }

    showResume () {
        this.shadowRoot.getElementById("showRes").classList.add("d-none")
        this.shadowRoot.getElementById("hideRes").classList.remove("d-none")
    }

    hideResume () {
        this.shadowRoot.getElementById("showRes").classList.remove("d-none")
        this.shadowRoot.getElementById("hideRes").classList.add("d-none")
    }

    getResumeData () {
        console.log("getResumeData")

        let xhr = new XMLHttpRequest()

        // El open no realiza la peticion AJAX, solo la inicializa a la request
        xhr.open("GET", "http://localhost:8081/ecobusiness/v1/statistics")

        // Prepara la funcion que se ejecutará cuando se reciba la respuesta de la llamada
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición getResumeData completada correctamente")
                console.log(JSON.parse(xhr.responseText))
                let APIResponse = JSON.parse(xhr.responseText)
                console.log(APIResponse)
                // el dato esta en .results dentro del response de esta peticion (esto cambia en cada peticion)
                this.stats = APIResponse
                this.stats.total = this.stats.fiveStarsBusiness + this.stats.fourStarsBusiness + this.stats.threeStarsBusiness + this.stats.twoStarsBusiness + this.stats.oneStarBusiness
            }
        }

        // Realiza la peticion AJAX
        xhr.send()
    }
}

customElements.define('persona-resumen', PersonaResumen)