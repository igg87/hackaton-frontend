import { LitElement, html, css } from 'lit-element'
import '../persona-rating/persona-rating.js'

class PersonaFichaListado extends LitElement {

    static get properties () {
        return {
            id: {type: String},
            name: {type: String},
            sector: {type: Object},
            registrationDate: {type: String},
            additionalInfo: {type: String},
            checks: {type: Array},
            photo: {type: Object},
            rating: {type: Number},
            changed: {type: Boolean}
        }
    }

    constructor () {
        super()

        this.checks = []
        this.sector = {}
        this.changePhoto()
    }

    static get styles () {
        return css`
            .marginAuto {
                margin-left: auto;
                margin-right: auto;
            }
            .opacity1 {
                opacity: 1 !important;
            }
            .bold {
                font-weight: bold;
            }
            .form-check-input {
                margin-top: 0px;
            }
            html, input, h5, p, label, a {
                font-family: Arial, Helvetica, sans-serif;
            }
        `
    }

    render () {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="h-100 mb-3">
                <div class="card">
                  <h5 class="card-header bold">${this.name} (<persona-rating rating="${this.rating}"></persona-rating>)</h5>
                  <div class="card-body">
                    <p><img src="${this.photo.src}" height="90" width="90" alt="${this.photo.alt}" class="marginAuto"></p>
                    <p class="card-text">${this.additionalInfo}</p>
                    <!--p class="card-text">Sector: ${this.sector.description}</p-->
                    <p class="card-text">Cliente desde: ${this.registrationDate}</p>
                    <div class="form-check">
                        ${this.checks.map(
                            checkSust => html`
                                <input class="form-check-input" id="check${checkSust.id}" type="checkbox" ?checked="${checkSust.active}" disabled>
                                <label class="form-check-label opacity1" for="flexCheckDefault">
                                    ${checkSust.description}
                                </label>
                                <br></br>
                            `
                        )}
                    </div>
                    <a @click="${this.deletePerson}" class="btn btn-danger">Eliminar</a>
                    <a @click="${this.moreInfo}" class="btn btn-primary">Modificar</a>
                  </div>
                </div>            
            </div>
        `
    }

    updated (changedProperties) {
        console.log("updated de ficha-listado")
        // Buscamos cambios en una variable concreta
        if (changedProperties.has("checks")) {
            console.log("Ha cambiado el checks en persona-ficha-listado")
            this.calcRating()   
        }
        if (changedProperties.has("sector")) {
            // console.log("Ha cambiado el sector en persona-ficha-listado")
            this.changePhoto()   
        }
        if (changedProperties.has("changed")) {
            console.log("Ha cambiado el changed en persona-ficha-listado")
            this.calcRating()
            for (var i = 0; i < this.checks.length; i++) {
                this.checks[i].active = this.shadowRoot.getElementById("check" + this.checks[i].id).checked
            }
            this.changed = false
        }
        
    }

    calcRating () {
        // console.log("calcRating")
        let number = this.checks.filter(
            sust => sust.active === true
        )
        this.rating = number.length
    }

    changePhoto () {
        if (this.sector.id === "2") {
            this.photo = {
                src: "./img/coche.png",
                alt: "Auto"
            }
        } else if (this.sector.id === "1") {
            this.photo = {
                src: "./img/bank.png",
                alt: "Banks"
            }
        } else {
            this.photo = {
                src: "./img/gamer.png",
                alt: "Others"
            }
        }
        
    }

    deletePerson (e) {
        console.log("deletePerson")
        console.log("Se va a borrar " + this.id)

        this.dispatchEvent(
            new CustomEvent("delete-person", {
                "detail": {
                    "id": this.id
                }
            })
        )
    }

    moreInfo (e) {
        console.log("moreInfo")
        console.log("Se ha pedido más información de la persona " + this.id)

        this.dispatchEvent(
            new CustomEvent("info-person", {
                "detail": {
                    "id": this.id
                }
            })
        )
    }
}

customElements.define('persona-ficha-listado', PersonaFichaListado)