import { LitElement, html, css } from 'lit-element'

class PersonaForm extends LitElement {

    static get properties () {
        return {
            person: {type: Object},
            sectors: {type: Array},
            editingPerson: {type: Boolean}
        }
    }

    constructor () {
        super()

        // this.listSustainable
        this.resetFormData()
        this.activeChecks()
        this.editingPerson = false
    }

    static get styles () {
        return css`
            .bold {
                font-weight: bold;
            }
            .form-check-input {
                cursor: pointer;
            }
            .form-check-input {
                margin-top: 0px;
            }
            html, input, h5, p, label, a, button, select, option, input {
                font-family: Arial, Helvetica, sans-serif;
            }
        `
    }

    render () {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="input-group mb-3">
                <form>
                    <input type="text" .value="${this.person.id}" ?disabled="true" class="form-control widthSmall" hidden="true" />

                    <div class="form-group mb-4">
                        <label class="bold">Nombre empresa</label>
                        <input type="text" @input="${this.updateName}" .value="${this.person.name}" class="form-control" placeholder="Nombre empresa" />
                    </div>

                    <div class="form-group mb-4">
                        <label class="bold">Sector</label>
                        <select @change="${this.updateSector}" class="form-select">
                            <option id="option0" value="0" > </option>
                            ${this.sectors.map(option => html`
                                <option id="option${option.id}" value="${option.id}" >${option.description}</option>
                            `)}
                        </select>
                    </div>

                    <div class="form-group mb-4">
                        <label class="bold">Otra información</label>
                        <input type="text" @input="${this.updateAdditionalInfo}" .value="${this.person.additionalInfo}" class="form-control" placeholder="Otra información" />
                    </div>

                    <div class="form-group mb-4">
                        <label class="bold">Fecha Alta</label>
                        <input type="text" maxlength="10"
                            @input="${this.updateRegistrationDate}"
                            .value="${this.person.registrationDate}"
                            class="form-control" placeholder="AAAA-MM-DD" />
                    </div>

                    <label class="bold">Indicadores de sostenibilidad</label>
                    <div class="form-check mb-4">
                        ${this.person.checks.map(
                            checkSust => html`
                                <input class="form-check-input" id="check${checkSust.id}" type="checkbox" ?checked="${checkSust.active}" >
                                <label class="form-check-label" for="flexCheckDefault">
                                    ${checkSust.description}
                                </label>
                                <br />
                            `
                        )}
                    </div>                    

                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `
    }

    updated (changedProperties) {
        // Buscamos cambios en una variable concreta
        if (changedProperties.has("editingPerson")) {
            console.log("Ha cambiado el editingPerson")
            this.activeChecks()
            this.activeSector()
        }
    }

    updateName (e) {
        this.person.name = e.target.value
    }
    updateSector (e) {
        let chosenSector = this.sectors.filter(
            sector => sector.id === e.target.value
        )
        console.log(chosenSector[0])
        this.person.sector = chosenSector[0]
    }

    updateAdditionalInfo (e) {
        this.person.additionalInfo = e.target.value
    }
    updateRegistrationDate (e) {
        this.person.registrationDate = e.target.value
    }

    goBack (e) {
        console.log("goBack")
        e.preventDefault()

        this.dispatchEvent(new CustomEvent("persona-form-close", {}))
        this.resetFormData()
    }

    storePerson (e) {
        console.log("storePerson")
        e.preventDefault()

        for (var i = 0; i < this.person.checks.length; i++) {
          this.person.checks[i].active = this.shadowRoot.getElementById("check" + this.person.checks[i].id).checked
        }

        if (this.editingPerson === true) {
            this.editPerson()
        } else {
            this.newPerson()
        }
    }

    editPerson () {
        console.log("- editPerson")

        let xhr = new XMLHttpRequest()
        // El open no realiza la peticion AJAX, solo la inicializa a la request
        xhr.open("PUT", "http://localhost:8081/ecobusiness/v1/business/" + this.person.id)
        xhr.setRequestHeader('Content-type', 'application/json');

        // Prepara la funcion que se ejecutará cuando se reciba la respuesta de la llamada
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("- Editado")
                // let APIResponse = JSON.parse(xhr.responseText)

                // el dato esta en .results dentro del response de esta peticion (esto cambia en cada peticion)
                this.eventStore()
            }
        }
        // Realiza la peticion AJAX
        xhr.send(JSON.stringify(this.person))
    }

    newPerson() {
        console.log("- newPerson")

        let xhr = new XMLHttpRequest()
        // El open no realiza la peticion AJAX, solo la inicializa a la request
        xhr.open("POST", "http://localhost:8081/ecobusiness/v1/business")
        xhr.setRequestHeader('Content-type', 'application/json');

        // Prepara la funcion que se ejecutará cuando se reciba la respuesta de la llamada
        xhr.onload = () => {
            if (xhr.status === 201) {
                console.log("- Creado")
                let APIResponse = JSON.parse(xhr.responseText)

                // el dato esta en .results dentro del response de esta peticion (esto cambia en cada peticion)
                this.person.id = APIResponse.business.id
                this.eventStore()
            }
        }
        // Realiza la peticion AJAX
        xhr.send(JSON.stringify(this.person))
    }

    eventStore () {
        console.log("eventStore")
        console.log(this.person)
        this.dispatchEvent(new CustomEvent("persona-form-store", {
            "detail": {
                "person": {
                    "id": this.person.id,
                    "name" : this.person.name,
                    "sector" : this.person.sector,
                    "registrationDate" : this.person.registrationDate,
                    "additionalInfo" : this.person.additionalInfo,
                    "checks": this.person.checks
                },
                "editingPerson": this.editingPerson
            }
        }))
        this.resetFormData()
    }

    resetFormData () {
        console.log("resetFormData")
        this.person = {
            "id": "0",
            "name" : "",
            "sector" : {},
            "registrationDate" : "",
            "additionalInfo" : "",
            "checks": []
        }
        // llamar a una funcion para recuperar el listado de valores
        // this.getChecks()
        /*
        this.person.checks = [
            { id: 1, description: 'Más del 50% de energía consumida de origen renovable', active: false},
            { id: 2, description: 'Dedicación de al menos el 5% de los beneficios a acciones sociales', active: false},
            { id: 3, description: 'Mujeres en puestos directivos de al menos 40%', active: false},
            { id: 4, description: 'Flota de vehículos eléctricos', active: false},
            { id: 5, description: 'Sin negocios con países que no respeten los derechos humanos', active: false}
        ]
        */
        this.getSectores()
        this.editingPerson = false
    }
    activeChecks () {
        for (let i = 0; i < this.person.checks.length; i++) {
            if (this.shadowRoot.getElementById("check" + this.person.checks[i].id) !== null) {
                this.shadowRoot.getElementById("check" + this.person.checks[i].id).checked = this.person.checks[i].active
            }
        }
    }
    activeSector () {
        // this.sectors[].id
        for (let i = 0; i < this.sectors.length; i++) {
            if (this.sectors[i].id === this.person.sector.id) {
                this.shadowRoot.getElementById("option" + this.sectors[i].id).selected = true
            } else {
                this.shadowRoot.getElementById("option" + this.sectors[i].id).selected = false
            }
            
        }
    }

    getSectores() {
        // console.log("getSectores")
    
        this.sectors = []
        let xhr = new XMLHttpRequest()
    
        // El open no realiza la peticion AJAX, solo la inicializa a la request
        xhr.open("GET", "http://localhost:8081/ecobusiness/v1/sectors")
    
        // Prepara la funcion que se ejecutará cuando se reciba la respuesta de la llamada
        xhr.onload = () => {
            if (xhr.status === 200) {
                // console.log("Petición completada correctamente")
                let APIResponse = JSON.parse(xhr.responseText)
                // console.log(APIResponse)
                // el dato esta en .results dentro del response de esta peticion (esto cambia en cada peticion)
                this.sectors = APIResponse
            }
        }

        // Realiza la peticion AJAX
        xhr.send()
    }

    /*
    getChecks() {
        console.log("-- getChecks()")
        this.person.checks = []
        let xhr = new XMLHttpRequest()
        
        // El open no realiza la peticion AJAX, solo la inicializa a la request
        xhr.open("GET", "http://localhost:8081/ecobusiness/v1/checks")
    
        // Prepara la funcion que se ejecutará cuando se reciba la respuesta de la llamada
        xhr.onload = () => {
            if (xhr.status === 200) {
                // console.log("Petición completada correctamente")
                let APIResponse = JSON.parse(xhr.responseText)
                // console.log(APIResponse)
                // el dato esta en .results dentro del response de esta peticion (esto cambia en cada peticion)
                this.person.checks = APIResponse
                this.activeChecks()
            }
        }

        // Realiza la peticion AJAX
        xhr.send()
    }  
    */  
}

customElements.define('persona-form', PersonaForm)