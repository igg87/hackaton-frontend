import { LitElement, html, css } from 'lit-element'

class PersonaFooter extends LitElement {

    static get properties () {
        return {}
    }

    constructor () {
        super()
    }

    static get styles () {
        return css`
            .customFooter {
                background-color: rgb(239 239 239);
                text-align: center;
                height: 25px;
                padding-top: 10px;
                margin-bottom: 0px;
                margin-top: 10px;
            }
            html, input, h5, p, label, a {
                font-family: Arial, Helvetica, sans-serif;
            }
        `
    }

    render () {
        return html`
            <h5 class="customFooter">Hackaton 2021 - Equipo 2</h5>
        `
    }
}

customElements.define('persona-footer', PersonaFooter)