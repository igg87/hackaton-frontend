import { LitElement, html, css } from 'lit-element'

class PersonaSidebar extends LitElement {

    constructor () {
        super()
    }

    static get styles () {
        return css`
            .ml-1 {
                margin-left: 5px;
            }
            html, input, h5, p, label, a {
                font-family: Arial, Helvetica, sans-serif;
            }
        `
    }

    render () {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <aside>
                <section>
                    <div class="mt-5 ml-1">
                        <button class="w-100 btn btn-success" style="font-size: 50px;"
                            @click="${this.newPerson}"><strong>+</strong></button>
                    </div>
                </section>
            </aside>
        `
    }

    newPerson () {
        console.log("newPerson en persona-sidebar")
        console.log("Se va a crear una nueva persona")

        this.dispatchEvent(
            new CustomEvent("new-person", {})
        )
    }
}

customElements.define('persona-sidebar', PersonaSidebar)