import { LitElement, html, css } from 'lit-element'
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'
import '../persona-resumen/persona-resumen.js'

class PersonaMain extends LitElement {

    static get properties () {
        return {
            people: {type: Array},
            showPersonForm: {type: Boolean},
            clickOnNewPerson: {type: Boolean},
            checks: {type: Array}
        }
    }

    constructor () {
        super()

        this.showPersonForm = false
        this.clickOnNewPerson = false
        this.people = []
        this.getPeopleData("http://localhost:8081/ecobusiness/v1/business")
        this.getChecks()
        /*
        if (Array.isArray(this.people) && this.people.length === 0) {
            this.setPeopleTest()
        }
        */
    }

    static get styles () {
        return css`
            .formCard {
                padding-top: 25px;
                padding-left: 25px;
            }
            .pl-1 {
                padding-left: 5px;
            }
            .pr-1 {
                padding-right: 5px;
            }
            html, input, h2, h5, p, label, a {
                font-family: Arial, Helvetica, sans-serif;
            }
        `
    }

    render () {
        // NOTA: Los parametros que pasamos por etiqueta deben ser string siempre. Si es un object, se pone un punto delante
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h2 class="text-center">Listado de clientes</h2>
            <persona-resumen id="resumenDiv"
                @persona-filter="${this.personaFilter}"
            ></persona-resumen>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4">
                    ${this.people.map(
                        person => html`
                            <div id="${person.id}" class="pl-1 pr-1">
                            <persona-ficha-listado
                                id="${person.id}"
                                name="${person.name}"
                                .sector="${person.sector}"
                                registrationDate="${person.registrationDate}"
                                additionalInfo="${person.additionalInfo}"
                                .checks="${person.checks}"
                                hasChanged="${person.changed}"
                                @delete-person="${this.deletePerson}"
                                @info-person="${this.infoPerson}"
                            ></persona-ficha-listado>
                            </div>
                        `
                    )}
                </div>
            </div>
            <div class="row">
                <persona-form
                    @persona-form-close="${this.personFormClose}"
                    @persona-form-store="${this.personFormStore}"
                    class="d-none border rounded col-11 col-md-8 col-lg-5 formCard"
                    id="personForm"
                ></persona-form>
            </div>
        `
    }

    updated (changedProperties) {
        console.log("updated")
        // Buscamos cambios en una variable concreta
        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el showPersonForm en persona-main")
            if (this.showPersonForm === true) {
                this.showPersonFormData()
            } else {
                this.showPersonList()
            }            
        }
        if (changedProperties.has("clickOnNewPerson")) {
            console.log("Ha cambiado el clickOnNewPerson en persona-main")
            console.log(this.clickOnNewPerson)
            if (this.clickOnNewPerson === true) {
                this.clickOnNewPerson = false
                console.log(this.shadowRoot.getElementById("personForm"))
                console.log(this.shadowRoot.getElementById("personForm").person)

                let personToShow = {}
                personToShow.id = "0"
                personToShow.name = null
                personToShow.sector = {}
                personToShow.registrationDate = null
                personToShow.additionalInfo = null
                personToShow.checks = this.checks
                this.shadowRoot.getElementById("personForm").person = personToShow
                this.shadowRoot.getElementById("personForm").editingPerson = false
            }       
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-main")
            console.log(this.people)

            this.dispatchEvent(new CustomEvent("updated-people", {
                    "detail": {
                        people: this.people
                    }
                }
            ))
        }
    }

    personaFilter (e) {
        console.log("personaFilter: " + e.detail.url)
        this.getPeopleData(e.detail.url)
    }

    getPeopleData (url) {
        console.log("getPeopleData")

        let xhr = new XMLHttpRequest()

        // El open no realiza la peticion AJAX, solo la inicializa a la request
        xhr.open("GET", url)

        // Prepara la funcion que se ejecutará cuando se reciba la respuesta de la llamada
        xhr.onload = () => {
            if (xhr.status === 200) {
                let APIResponse = JSON.parse(xhr.responseText)
                console.log(APIResponse)
                // el dato esta en .results dentro del response de esta peticion (esto cambia en cada peticion)
                this.people = APIResponse
                for (let i=0;i<this.people.length;i++) {
                    this.people[i].changed = false
                }
            } 
        }

        // Realiza la peticion AJAX
        xhr.send()
    }

    // Funcion para generar clientes cuando no tenemos el back levantado
    /*
    setPeopleTest() {
        console.log("-- setPeopleTest")
        this.person = {
            "id": "0",
            "name" : "Cliente de prueba",
            "sector" : { "id": "2", "description": "Auto", "isActive": true },
            "registrationDate" : "2021-03-08",
            "additionalInfo" : "Texto libre",
            "checks": [],
            "changed": false
        }
        // llamar a una funcion para recuperar el listado de valores
        this.person.checks = [
            { id: 1, description: 'Más del 50% de energía consumida de origen renovable', active: true},
            { id: 2, description: 'Dedicación de al menos el 5% de los beneficios a acciones sociales', active: false},
            { id: 3, description: 'Mujeres en puestos directivos de al menos 40%', active: true},
            { id: 4, description: 'Flota de vehículos eléctricos', active: false},
            { id: 5, description: 'Sin negocios con países que no respeten los derechos humanos', active: false}
        ]
        this.people[0] = this.person
        this.people[1] = this.person
        this.people[2] = this.person
        this.people[3] = this.person
        this.people[4] = this.person
    }
    */


    deletePerson (e) {
        console.log("deletePerson - persona-main")
        console.log("Se va a borrar la persona de id " + e.detail.id)

        let xhr = new XMLHttpRequest()

        // El open no realiza la peticion AJAX, solo la inicializa a la request
        xhr.open("DELETE", "http://localhost:8081/ecobusiness/v1/business/" + e.detail.id)

        // Prepara la funcion que se ejecutará cuando se reciba la respuesta de la llamada
        xhr.onload = () => {
            if (xhr.status === 200) {
                console.log("Petición completada correctamente")
                // La funcion filter genera un Array(eliminando el recibido) y se vuelve a asignar a this.people
                this.people = this.people.filter(
                    person => person.id != e.detail.id
                )
                this.refreshDataResumen()
            }
        }

        // Realiza la peticion AJAX
        xhr.send()
    }

    infoPerson (e) {
        console.log("infoPerson - persona-main")
        console.log(e.detail)

        let chosenPerson = this.people.filter(
            person => person.id === e.detail.id
        )
        console.log(chosenPerson)

        let personToShow = {}
        personToShow.id = chosenPerson[0].id
        personToShow.name = chosenPerson[0].name
        personToShow.sector = chosenPerson[0].sector
        personToShow.registrationDate = chosenPerson[0].registrationDate
        personToShow.additionalInfo = chosenPerson[0].additionalInfo
        personToShow.checks = chosenPerson[0].checks

        this.shadowRoot.getElementById("personForm").person = personToShow
        this.shadowRoot.getElementById("personForm").editingPerson = true
        this.showPersonForm = true
    }

    showPersonFormData () {
        console.log("showPersonFormData")
        this.shadowRoot.getElementById("personForm").classList.remove("d-none")
        this.shadowRoot.getElementById("peopleList").classList.add("d-none")
        this.shadowRoot.getElementById("resumenDiv").classList.add("d-none")
    }

    showPersonList () {
        console.log("showPersonList")
        this.shadowRoot.getElementById("personForm").classList.add("d-none")
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none")
        this.shadowRoot.getElementById("resumenDiv").classList.remove("d-none")
    }

    personFormClose () {
        console.log("personFormClose")
        this.showPersonForm = false
    }

    personFormStore (e) {
        console.log("personFormStore")
        console.log("Se va a almacenar una persona")

        console.log("La propiedad name en person vale " + e.detail.person.name)
        console.log("La propiedad editingPerson vale " + e.detail.editingPerson)

        if (e.detail.editingPerson === true) {
            console.log("Se va a actualizar la persona de nombre " + e.detail.person.name)

            this.people = this.people.map(
                person => person.id === e.detail.person.id ? person = e.detail.person : person
            )

            // Se marcha la persona como changed para recargar el calculo de ratings y checks
            let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            )
            if (indexOfPerson >= 0) {
                this.shadowRoot.getElementById(this.people[indexOfPerson].id).querySelector("persona-ficha-listado").changed = true
            }

        } else {
            console.log("Se va a almacenar una persona nueva")
            // Se refactoriza para que this.people cambie y salga por el updated
            // this.people.push(e.detail.person)
            this.people = [...this.people, e.detail.person]
        }
        console.log("Persona almacenada")
        this.refreshDataResumen()
        this.showPersonForm = false
    }

    refreshDataResumen() {
        console.log("-- refreshDataResumen")
        this.shadowRoot.getElementById("resumenDiv").changed = true
    }

    getChecks() {
        console.log("-- getChecks()")
        this.checks = []
        let xhr = new XMLHttpRequest()
        
        // El open no realiza la peticion AJAX, solo la inicializa a la request
        xhr.open("GET", "http://localhost:8081/ecobusiness/v1/checks")
    
        // Prepara la funcion que se ejecutará cuando se reciba la respuesta de la llamada
        xhr.onload = () => {
            if (xhr.status === 200) {
                // console.log("Petición completada correctamente")
                let APIResponse = JSON.parse(xhr.responseText)
                // console.log(APIResponse)
                // el dato esta en .results dentro del response de esta peticion (esto cambia en cada peticion)
                this.checks = APIResponse
            }
        }

        // Realiza la peticion AJAX
        xhr.send()
    }  
}

customElements.define('persona-main', PersonaMain)