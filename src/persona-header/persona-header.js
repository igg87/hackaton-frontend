import { LitElement, html, css } from 'lit-element'

class PersonaHeader extends LitElement {

    static get properties () {
        return {}
    }

    constructor () {
        super()
    }

    static get styles () {
        return css`
            .customHeader {
                margin-top: 0px;
                background-color: #a4d8f7;
                text-align: center;
                height: 55px;
                padding-top: 15px;
            }
            html, input, h1, h5, p, label, a {
                font-family: Arial, Helvetica, sans-serif;
            }
        `
    }

    render () {
        return html`
            <h1 class="customHeader">Eco-Business</h1>
        `
    }
}

customElements.define('persona-header', PersonaHeader)